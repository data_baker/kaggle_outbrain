# Outbrain Click Prediction
https://www.kaggle.com/c/outbrain-click-prediction


[TOC]

### bis zum nächsten Mal
Malte: AWS
Levin: Daten einlesen
Birgit: Rundmail
Henning: Kernel ausführen u Submission machen


### Arbeitsweise
2-3 mal im Monat treffen


### to dos
- Kernels angucken
- mit welchen Daten fangen wir an? (Erste Explorative Datenanalyse)
- Welche Cloud-Lösung bietet sich für uns an und ist praktisch. Infos sammeln.


### offene Fragen:
- wird in jedem ad-set einmal oder auch mehrmals oder keinmal geklickt?
- was sind entities?
- lagern wir die Daten in der Cloud? (auf einer runterfahrbaren Instanz bspw)
- Gibt es eine IDE für Python, die man mit AWS verwenden kann?

### Python Distribution
PyCharm
https://www.continuum.io/downloads


### Wissen aus dem Forum sammeln
Gern hier eine Kurzzusammenfassung (am besten mit Link posten)

- Testdaten sind offenbar nicht völlig zufällig über den Zeitraum verteilt: https://www.kaggle.com/joconnor/outbrain-click-prediction/date-exploration-and-train-test-split
- interessante EDA: https://www.kaggle.com/anokas/outbrain-click-prediction/outbrain-eda/discussion