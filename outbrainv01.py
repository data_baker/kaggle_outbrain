

import os
import numpy as np      # math operations and array
import pandas as pd     # data frame and exploratory tools
import sklearn as skl   # machine learning basics and metrics
import xgboost as xgb   # gradient boosting forest package
import keras as krs     # neural network package
import matplotlib.pyplot as plt     # for easy plotting
import random as rd     # pseudo random generator
from sklearn import model_selection # separating training and test data
import math             # some math operations
from sklearn import grid_search     # good for tuning


events = pd.read_csv('../input/events.csv')
pageviews = pd.read_csv('../input/page_views_sample.csv')
promoted_content = pd.read_csv('../input/promoted_content.csv')
clicks_train = pd.read_csv('../input/clicks_train.csv')
clicks_test = pd.read_csv('../input/clicks_test.csv')

clicks_train.merge(events,on='display_id',how='left')
clicks_train.join(pageviews,on='document_id',how='left')
